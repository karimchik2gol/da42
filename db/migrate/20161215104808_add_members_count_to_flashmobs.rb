class AddMembersCountToFlashmobs < ActiveRecord::Migration
  def change
    add_column :flashmobs, :members_count, :integer, default: 0
  end
end
