class AddStripTagToTextdescs < ActiveRecord::Migration
  def change
    add_column :textdescs, :strip_tags, :boolean, default: false
  end
end
