class AddExpiresAtToFlashmobs < ActiveRecord::Migration
  def change
    add_column :flashmobs, :expires_at, :datetime
  end
end
