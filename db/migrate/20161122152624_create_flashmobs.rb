class CreateFlashmobs < ActiveRecord::Migration
  def change
    create_table :flashmobs do |t|
      t.string :name
      t.text :short_description
      t.text :description
      t.integer :reports_count, default: 0
      t.string :status
      t.integer :user_id
      t.string :gender
      t.integer :start_age
      t.integer :end_age

      t.timestamps null: false
    end
  end
end
