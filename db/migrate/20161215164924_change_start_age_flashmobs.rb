class ChangeStartAgeFlashmobs < ActiveRecord::Migration
  def change
  	change_column :flashmobs, :start_age, :integer, default: 1
  	change_column :flashmobs, :end_age, :integer, default: 100
  end
end
