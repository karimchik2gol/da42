class AddLocationToFlashmobs < ActiveRecord::Migration
  def change
    add_column :flashmobs, :location, :string
  end
end
