class CreateTextdescs < ActiveRecord::Migration
  def change
    create_table :textdescs do |t|
      t.string :name
      t.string :classname
      t.text :description

      t.timestamps null: false
    end
  end
end
