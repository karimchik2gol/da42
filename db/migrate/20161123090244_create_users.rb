class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone
      t.string :rank, default: 0
      t.string :image

      t.timestamps null: false
    end
  end
end
