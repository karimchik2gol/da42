class AddSlugToFlashmobs < ActiveRecord::Migration
  def change
    add_column :flashmobs, :slug, :string
    add_index :flashmobs, :slug, unique: true
  end
end
