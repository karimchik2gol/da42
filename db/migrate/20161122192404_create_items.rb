class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :flashmob_id
      t.integer :member_id

      t.timestamps null: false
    end
  end
end
