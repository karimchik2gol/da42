class AddFlashmobIdToMember < ActiveRecord::Migration
  def change
    add_column :members, :flashmob_id, :integer
  end
end
