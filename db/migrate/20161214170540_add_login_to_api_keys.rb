class AddLoginToApiKeys < ActiveRecord::Migration
  def change
    add_column :api_keys, :login, :string
    add_column :api_keys, :password, :string
  end
end
