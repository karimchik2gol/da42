require 'test_helper'

class FlashmobsControllerTest < ActionController::TestCase
  setup do
    @flashmob = flashmobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:flashmobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create flashmob" do
    assert_difference('Flashmob.count') do
      post :create, flashmob: { description: @flashmob.description, end_age: @flashmob.end_age, gender: @flashmob.gender, name: @flashmob.name, reports_count: @flashmob.reports_count, short_description: @flashmob.short_description, start_age: @flashmob.start_age, status: @flashmob.status, user_id: @flashmob.user_id }
    end

    assert_redirected_to flashmob_path(assigns(:flashmob))
  end

  test "should show flashmob" do
    get :show, id: @flashmob
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @flashmob
    assert_response :success
  end

  test "should update flashmob" do
    patch :update, id: @flashmob, flashmob: { description: @flashmob.description, end_age: @flashmob.end_age, gender: @flashmob.gender, name: @flashmob.name, reports_count: @flashmob.reports_count, short_description: @flashmob.short_description, start_age: @flashmob.start_age, status: @flashmob.status, user_id: @flashmob.user_id }
    assert_redirected_to flashmob_path(assigns(:flashmob))
  end

  test "should destroy flashmob" do
    assert_difference('Flashmob.count', -1) do
      delete :destroy, id: @flashmob
    end

    assert_redirected_to flashmobs_path
  end
end
