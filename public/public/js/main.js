
// PREVENT CLICK
$('body').on('click', 'a', function(e){
  if( $(this).attr('href')=='' || $(this).attr('href')=='#')
    e.preventDefault();
});

function clear_errors(th){  
  th.find('.error-border').removeClass('error-border');
  th.find('.error').empty();
}

// Error...
inputs = ["input", "textarea", "select"];
function set_errors(name, data, th){
  var errors = $.parseJSON(data.responseText);
  $.each(errors, function(index, val) {
    $.each(val, function(key, value) {
      base_key = name + '[' + index + ']';
      nm = "[name='" + base_key + "']";
      for(i = 0; i < inputs.length; i++) {
        el = th.find(inputs[i] + nm);
        if(el.length) break;
      }


      el.addClass('error-border')
      el.next().text(index + ' ' +value)
    });
  });
}

// CHECKBOX VAL
$('input[type="checkbox"]').change(function(){
  $(this).val($(this).is(':checked'));
})

// REGISTRATION
$('#ModalSignup form').submit(function(e){
  e.preventDefault();
  th = $(this);
  clear_errors(th);

  $.ajax({
    dataType: 'JSON',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/users',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      success_login_or_register(th, 'ModalSignup', data, 'user');
    },
    error: function(data) {
      set_errors('user', data, th);
    }
  });
});

$('#ModalLogin form').submit(function(e){
  e.preventDefault();
  th = $(this);
  th.find('.error').empty();

  $.ajax({
    dataType: 'JSON',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/sessions',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      success_login_or_register(th, 'ModalLogin', data, 'user');
    },
    error: function(data) {
      var errors = $.parseJSON(data.responseText);
      th.find('.error').text(errors);
    }
  });
})

function get_sign_in_panel(par){
  $.ajax({
    dataType: 'HTML',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/profile/get_sign_in_panel',
    success: function(data) {
      par.append(data);
    }
  })
}

function success_login_or_register(th, id, data, role){
  th.find('input').val('');
  $('#' + id + ' .close').click();
  if(role == "user") render_menu();

  $.each(data, function(index, value){
    inpt = th.find('input[name="user[' + index + ']"]')
    inpt.val(value);
  });
}

function render_menu(){
  menu = $('.user-login-register');
  par = menu.parent();
  menu.remove();
  get_sign_in_panel(par); // append new panel
}
// REGISTRATION

// SET TEXTDESC INFO
function set_data_from_controller(){
  info = $('.info-from-controller').data('info');
  $('.info-from-controller').remove();
  $.each(info, function(index, value){
    classname = value['classname'];
    content = value['description']
    $(classname).append(content);
  });
}
set_data_from_controller();
// SET TEXTDESC INFO

// CONTACT FORM
$('.contact-form form').submit(function(e){
  e.preventDefault();
  th = $(this);
  clear_errors(th);

  $.ajax({
    async: true,
    dataType: 'JSON',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/contact',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      th.find('input, textarea').val('');
      th.find('input[type="submit"]').val('SEND MESSAGE');
    },
    error: function(data) {
      set_errors('feedback', data, th)
    }
  });
});
// CONTACT FORM

// EDIT FORM
$('#ModalEdit form').submit(function(e){
  e.preventDefault();
  th = $(this);
  clear_errors(th);

  $.ajax({
    dataType: 'JSON',
    type: 'PUT',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/users',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      success_login_or_register(th, 'ModalSignup', data, 'user');
    },
    error: function(data) {
      set_errors('user', data, th);
    }
  });
})
// EDIT FORM

// FLASHMOBS
/* ПЛАНИРОВАЛ СДЕЛАТЬ АЯКСОМ, НО СОРРИ, УЕБИЩНЫЙ ДИЗАЙН */
QUERY = {}
function collect_query(th){
  $.each(th.find('input, select'), function(){
    QUERY[$(this).attr('name')] = $(this).val(); 
  });
}

function show_load_button(status) {
  if(status == 200)
    $('.load_more_btn a').show();
  else
    $('.load_more_btn a').hide();
}

$('.search').submit(function(e){
  e.preventDefault();
  th = $(this);
  collect_query(th);

  QUERY['page'] = 1;
  PAGE = 2;

  $.ajax({
    dataType: 'HTML',
    type: 'GET',
    url: '/flashmobs/search',
    data: QUERY,
    success: function(data, textStatus, xhr) {
      $('.flashmobs-ul').empty();
      $('.flashmobs-ul').append(data);
      show_load_button(xhr.status);
    }
  });
})

// LOAD BUTTON
PAGE = 2;
$('.load_more_btn a').click(function(e){
  e.preventDefault();
  th = $(this);
  collect_query($('.search'));
  
  QUERY['page'] = PAGE

  $.ajax({
    dataType: 'HTML',
    type: 'GET',
    data: QUERY,
    url: '/flashmobs/search',
    success: function(data, textStatus, xhr) {
      $('.flashmobs-ul').append(data);
      show_load_button(xhr.status);
      if (xhr.status == 200) PAGE += 1;
    }
  });
});
// FLASHMOBS

// MEMBERS!!! YRAAA
function comment_or_member(id){
  setTimeout(function(){
    $('.comment-or-member').empty();
    $.ajax({
      async: false,
      dataType: 'HTML',
      type: 'GET',
      url: '/flashmobs/com', // comment or member
      data: {'id': id},
      success: function(data) {
        $('.comment-or-member').append(data);
      }
    });
  }, 500); // Delay for our favourite shadow for popup <333333
  
}

$('#JoinUs form').submit(function(e){
  e.preventDefault();
  th = $(this);
  clear_errors(th);

  $.ajax({
    dataType: 'JSON',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/members',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      id = th.find('input[type="hidden"]').val();
      success_login_or_register(th, 'JoinUs', data, 'member');
      comment_or_member(id);
    },
    error: function(data) {
      set_errors('member', data, th);
    }
  });
});

function show_comment_form(ths, e){
  e.preventDefault();
  th = jQuery(ths);
  par = th.parent();
  par.hide();
  par.next().show();

}

// COMMENTS !!!!
function get_comment(id) {
  var text;
  $.ajax({
    async: false,
    dataType: 'HTML',
    type: 'GET',
    url: '/comments/' + id,
    success: function(data, textStatus, xhr) {
      text = data;
    }
  });
  return text;
}

function send_comment(ths, e) {
  e.preventDefault();
  th = jQuery(ths);
  clear_errors(th);

  $.ajax({
    dataType: 'JSON',
    type: 'POST',
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    url: '/comments',
    data: th.serialize(),
    success: function(data, textStatus, xhr) {
      comment = get_comment(data);
      $('.comments-list').prepend(comment);
      th.find("textarea").val("");
    },
    error: function(data) {
      set_errors('comments', data, th);
    }
  });
}

function link_to_add_fields_style(){
  el = $('.link_to_add_fields');
  el.attr('class', 'btn btn-block new_flashmob link_to_add_fields');
}