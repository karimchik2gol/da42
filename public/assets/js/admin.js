jQuery(document).ready(function() {
	$("input[type='checkbox']").change(function(){
		$(this).parent().find("input[type='hidden']").val($(this).is(":checked"));
	})

	$(".change-checkbox").click(function(){
		$(this).find("input[type='checkbox']").trigger("change");
	})
})