json.extract! feedback, :id, :name, :email, :phone, :subject, :text, :created_at, :updated_at
json.url feedback_url(feedback, format: :json)