json.extract! user, :id, :name, :phone, :rank, :image, :created_at, :updated_at
json.url user_url(user, format: :json)