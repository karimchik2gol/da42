json.extract! member, :id, :name, :email, :phone, :age, :gender, :about, :flashmob_id, :password, :created_at, :updated_at
json.url member_url(member, format: :json)