json.extract! textdesc, :id, :name, :classname, :description, :created_at, :updated_at
json.url textdesc_url(textdesc, format: :json)