module ApplicationHelper
	include CarrierWave::MiniMagick

	def file_field_with_default(obj, fieldName, opts = {})
		data = {"height" => opts[:height] ? opts[:height] : 300 }
		if obj && obj[fieldName] && has_file?(obj.send("#{fieldName}"))
			data["default-file"] = obj.send("#{fieldName.to_s}_url") 
		end
		return data
	end

	def link_to_add_fields(name, f, association)
	    new_object = f.object.class.reflect_on_association(association).klass.new
	    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
	     	render('admin/flashmobs/' + association.to_s.singularize + "_fields", :locals => {:form => builder})
	    end
	    button_tag("#", "type" => "button", "data-association" => "#{association}" , "data-content" => "#{fields}", :class => "link_to_add_fields btn btn-info waves-effect waves-light btn-md" ) do
	    	name
	    end
	end

	def first_image(flashmob)
		fix_errors(flashmob) {
			flashmob.images.sort do |a, b| 
				a, b = get_size_two_images(a, b)
				b[:width] * b[:height] <=> a[:width] * a[:height]
			end.first
		}
	end

	def min_image_where_width_larger_than_height(flashmob)
		fix_errors(flashmob) {
			sorted = flashmob.images.sort do |a, b|
				a, b = get_size_two_images(a, b)
				a[:width] * a[:height] <=> b[:width] * b[:height]
			end
			sorted.delete_if do |a|
				img = get_size(a.image_url)
				a if img[:width] < img[:height]
			end.first
		}
	end

	def flashmobs_image(flashmob)
		fix_errors(flashmob) {
			img = flashmob.images.to_a.delete_if do |a|
				img = get_size(a.image_url)
				a if img[:width] - img[:height] > 180
			end.first

			return img || min_image_where_width_larger_than_height(flashmob)
		}
	end

	def comment_or_member(flashmob)
		if session[:flashmob_id] && session[:flashmob_id].include?(flashmob.id)
			name = 'write_comment'
		else
			name = 'become_member'
		end
		render partial: 'index/' + name, locals: { :flashmob => flashmob }
	end

	private
	def fix_errors(flashmob)
		begin
			yield(flashmob)
		rescue
			flashmob.images.first
		end
	end

	def get_size_two_images(a, b)
		a = get_size(a.image_url)
		b = get_size(b.image_url)
		return a, b
	end

	def get_size(image)
		a_url = get_image_url(image)
		a = MiniMagick::Image.open(a_url)
		return a
	end

	def get_image_url(image)
		if File.exists?(image)
			url = image
		else
			url = "#{Rails.root}/public" + image
		end
	end

	def has_file?(image)
		begin
	  		return image && image.file &&  image.file.exists?
	  	rescue
	  		return false
	  	end
	end
end
