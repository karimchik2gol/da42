class Textdesc < ActiveRecord::Base
	include BeautifulDate
	include MyValidator

	before_save :strip

	#validation
	validates :name, :presence => true, :uniqueness => true
	validates_each :description do |record, attr, value|
    if(remove_tags_from_text_and_still_empty?(record, attr, value)) # Method destroys tags from text
      record.errors.add(attr.to_sym, "Can't be empty") 
    end
	end

  def strip
  	self.description = Textdesc.strip_html_tags(description).gsub(/s+/, " ") if strip_tags
  end
end
