class User < ActiveRecord::Base
	include Password
	include Rank
 	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
 	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	mount_uploader :image, AdminUploader
	has_many :flashmobs, dependent: :destroy
	has_one :api_key

	validates :name, presence: true
	validates :phone, format: { with: /\A\d{8,14}\z/ }, uniqueness: true # only numbers
	validates :rank, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: RANK.count - 1 }
end
