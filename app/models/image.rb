class Image < ActiveRecord::Base
	include ImageManipulation
	belongs_to :flashmob, inverse_of: :images

	validates_presence_of :image

	mount_uploader :image, GalleryUploader
end
