module MyValidator
    extend ActiveSupport::Concern

    module ClassMethods
	    def remove_tags_from_text_and_still_empty?(record, attr, value)
	      obj = record[attr.to_sym] # Define name of attribute
	      # Removing tags
	      sanitizedText = format_text(obj) if obj
	      return true if !obj || sanitizedText.empty?
	      return false
	    end

	    def format_text(obj)
	      strip_html_tags(obj).gsub(/(\&nbsp\;|\&\#39\;|\s+|\u00A0)/, "")
	    end
	    
	    def strip_html_tags(obj)
	    	ActionView::Base.full_sanitizer.sanitize(obj)
	    end
	end
end