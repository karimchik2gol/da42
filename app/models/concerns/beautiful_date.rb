module BeautifulDate
    extend ActiveSupport::Concern

	def beautiful_created_at
		self.created_at.strftime("%d/%m/%Y")
	end

	def beautiful_updated_at
        self.created_at.strftime("%d/%m/%Y")
    end
end