module Password
    extend ActiveSupport::Concern

  	module ClassMethods
   		# Removing an empty password from validation
		def reset_password(par)
	  		pass = par[:password]
	  		if(pass && pass.gsub(/\s+/, "").empty?)
	  			par.tap { |hs| hs.delete(:password) }
	  			par.tap { |hs| hs.delete(:password_confirmation) }
	  		end
	  		par
	  	end
	end
end