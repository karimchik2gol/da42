module Rank
	extend ActiveSupport::Concern
	RANK = {
		"0" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"1" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"2" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"3" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"4" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"5" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"6" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"},
		"7" => {:title => "Cowboy", :image => "/images/icons/rank/image_for_cowboy.png"}
	}
end