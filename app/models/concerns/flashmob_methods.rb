#coding:UTF-8
module FlashmobMethods
    extend ActiveSupport::Concern
	STATUS = [
		"Declined",
		"Pending",
		"Approved"
	]

	GENDER = [
		"Man",
		"Woman"
	]

	included do 
	    # Default min image_count
    	$default_image_count = 3	

	    # Default height for image data
	    $height = 100

		# Methods
		# Get default or existed images tag
		def load_pictures(action)
			data = []
			if action == "edit" && images.count > $default_image_count - 1
				images.each do |f|
					startHash = {height: $height}
					if Image.has_file?(f.image)
						startHash["default-file"] = f.image_url
					end
					data << startHash
				end
			else
				$default_image_count.times { data << {height: $height} }
			end
			data
		end

		# Initialize each image and then valid it
		# Push to collection

		# If images dont exist set default image count to 3
		def count_of_images
			(images && images.count) || $default_image_count 
		end

	    # Reject if image is missing, methods 
	    def my_build_images(flashmob)
	    	imgCount = images_count_before_save(flashmob)
		    if imgCount < 3
		    	build_images(3 - imgCount)
		    end
	    end

	    def images_count_before_save(flashmob)
	    	imgsCount = 0
	    	flashmob[:images_attributes].each do |f|
		        unless reject_image_if_empty_or_update(f[1])
		          imgsCount += 1
		        end
		    end
		    imgsCount
	    end

	    def reject_image_if_empty_or_update(a)
			img = a[:image]
			missingImage = !img || !img.as_json["original_filename"] || img.as_json["original_filename"].blank?
			missingImage && a[:id].nil? && (a["image_cache"].nil? || a["image_cache"].blank?)
	    end

	    def nested_count_valid?(min, max, obj, name)
	      	add_nested_count_error(name, min, max) unless obj.reject(&:marked_for_destruction?).count >= min && obj.count <= max
	    end

	    # Building default values
	    # For images
	    def build_images(couns=3)
	      	couns.times { images.build } if images.count < 3
	    end

	    # PRIVATE
	    private
	    def add_nested_count_error(obj, min, max)
	      errors.add(obj, "Minimum #{min} #{obj.to_s} and maximum #{max}")
	    end
	end
end