module ImageManipulation
    extend ActiveSupport::Concern
    # sybmName is an attribute in Database
    # Format passed in the methods is symbol

    included do 
		before_validation :remove_if_dropify_is_true

		def _dropify_delete=(attributes)
			@_dropify_delete = attributes
		end

		private
		def remove_if_dropify_is_true
			if @_dropify_delete == "true"
				remove_image! 
			end
		end
	end
end