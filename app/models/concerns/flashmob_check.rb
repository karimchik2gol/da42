module FlashmobCheck
    extend ActiveSupport::Concern

    included do
	    def cflash
			flashmob = Flashmob.find(self.flashmob_id)

			# If flashmob exists and approved
			unless flashmob or !flashmob.approved?
				errors.add(:flashmob_id, "Flashmob doesn't exist")
			end
			return flashmob
	    end
	end
end