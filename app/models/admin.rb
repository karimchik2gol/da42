class Admin < ActiveRecord::Base
    include Password
    include BeautifulDate
  	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

    mount_uploader :image, AdminUploader  	
end
