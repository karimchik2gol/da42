class Comment < ActiveRecord::Base
	include FlashmobCheck
    include BeautifulDate

    scope :last_comments, ->(lim = 5) { order('created_at DESC').limit(5) }

	belongs_to :flashmob
	belongs_to :member

	before_validation :remove_blank_spaces
	validates :text, presence: true, length: { in:0..250 }, allow_blank: false
	validates :member_id, :flashmob_id, presence: true

	validate :check_flashmob

	private
	def check_flashmob
		flashmob = cflash
		member = Member.find(member_id)
		if !member || member.flashmob_id != flashmob.id

		end
	end

	def remove_blank_spaces
		self.text = text.gsub(/\s+/, " ")
	end
end
