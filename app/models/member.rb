class Member < ActiveRecord::Base
	include FlashmobCheck
	include FlashmobMethods
  	include MyValidator
	has_many :comments, dependent: :destroy
	has_one :api_key
	belongs_to :flashmobs

	validates :name, :flashmob_id, :phone, presence: true
	validates :email, presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }
	#validates :phone, format: { with: /[+]\d{,2}\(\d{3}\)-(\d{3})-(\d{2})-(\d{2})/ } # +38(066)-801-18-89
	current_year = Time.now.strftime("%Y").to_i

	validate :check_flashmob

	private
	def check_flashmob
		flashmob = cflash

		# If user hadn't registered yet
		mes_unique = "has already exist in this flashmob"
		if flashmob && member_query("email", self.email).count > 0
			errors.add(:email, mes_unique)
		elsif flashmob && member_query("phone", self.phone).count > 0
			errors.add(:phone, mes_unique)
		end

		# If gender is not some shit
		unless GENDER.include?(self.gender)			
			errors.add(:gender, "Nice gender man")
		end

		return unless flashmob
		# Return if flashmob doesn't exist else
		# Check flashmob gender and member gender
		gender_flash = flashmob.gender
		unless gender_flash.empty? || gender_flash == self.gender
			errors.add(:gender, "are not #{gender_flash.downcase}, sorry")
		end

		# Check start_age and end_age
		current_year = Time.now.strftime("%Y").to_i
		start_age = flashmob.start_age || 1
		end_age = flashmob.end_age || 99
		your_age = current_year - self.age
		if (your_age < start_age) || (your_age > end_age)
			errors.add(:age, "is unsuitable for us")
		end
	end

	def member_query(key, val)
		Member.where("flashmob_id = ? and #{key} = ?", self.flashmob_id, val)
	end
end
