require 'bcrypt'
class ApiKey < ActiveRecord::Base
	belongs_to :user
	belongs_to :member
	before_save :generate_access_token

	private
	# def set_password_bcrypt
	# 	self.password = BCrypt::Password.create(self.password)
	# end

	def generate_access_token
		# GENERATE ACCESS TOKEN UNTIL IT'S UNIQUE
	  	begin
	    	self.access_token = SecureRandom.hex
	  	end while self.class.exists?(access_token: access_token) 
	end
end
