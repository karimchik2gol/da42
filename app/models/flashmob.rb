class Flashmob < ActiveRecord::Base
	include BeautifulDate
	include MyValidator
	include FlashmobMethods
	extend FriendlyId

	ITEMS_ON_PAGE = 1

  	friendly_id :translited_name, use: :slugged

  	before_create :set_status

	scope :accessed, -> { where(:status => 'Approved') }
	scope :popular, ->(lim = 5) { order('members_count DESC').limit(lim) }
	scope :lastflash, ->(lim = 5) { order('created_at DESC').limit(lim) }

	belongs_to :user
	has_many :images, inverse_of: :flashmob, dependent: :destroy
	has_many :members, dependent: :nullify
	has_many :comments, dependent: :destroy
	accepts_nested_attributes_for :images, allow_destroy: true, :reject_if => :reject_image_if_empty_or_update

  	validates_associated :images
	validate :check_images_number
	validates :status, inclusion: { in: STATUS }, :allow_blank => true
	validates :gender, inclusion: { in: GENDER }, :allow_blank => true 
	validates :name, :location, :expires_at, presence: true
	validates :reports_count, :start_age, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true
	validates :end_age, numericality: { less_than_or_equal_to: 100 }, allow_nil: true


	# Validation each text type column
	validates_each :description, :short_description do |record, attr, value|
		# Method destroys tags from text
	    if(remove_tags_from_text_and_still_empty?(record, attr, value)) 
	      record.errors.add(attr.to_sym, "Can't be empty") 
	    end
	end

	def approved?
		self.status == "Approved"
	end

	def beautiful_expires_at
		self.expires_at.strftime("%d/%m/%Y")
	end

	private
	def set_status
		self.status = STATUS[1]	
	end

	def check_images_number
		nested_count_valid?(3, 10, images, :images)
	end

	def translited_name
		Translit.convert(name, :english)
	end
end
