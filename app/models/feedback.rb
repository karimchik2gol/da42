class Feedback < ActiveRecord::Base
	include BeautifulDate
	include MyValidator
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

	before_save :strip

	validates_presence_of :name, :email, :subject, :text, :phone
	validates :phone, format: { with: /\A\d{8,14}\z/ } # only numbers
	validates :email, :format => {:with => VALID_EMAIL_REGEX, :message => "Invalid email"}
	validates :text, length: { :maximum => 250 }

	def strip
  		self.text = Textdesc.strip_html_tags(text).gsub(/s+/, " ")
  	end
end
