// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require datatables/jquery.dataTables.min.js
//= require datatables/dataTables.bootstrap.js
//= require datatables/dataTables.buttons.min.js
//= require datatables/buttons.bootstrap.min.js
//= require datatables/jszip.min.js
//= require datatables/pdfmake.min.js
//= require datatables/vfs_fonts.js
//= require datatables/buttons.html5.min.js
//= require datatables/buttons.print.min.js
//= require datatables/dataTables.fixedHeader.min.js
//= require datatables/dataTables.keyTable.min.js
//= require datatables/dataTables.responsive.min.js
//= require datatables/responsive.bootstrap.min.js
//= require datatables/dataTables.scroller.min.js

//= require pages/datatables.init.js