// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require basic/bootstrap.min.js
//= require basic/detect.js
//= require basic/fastclick.js
//= require basic/jquery.slimscroll.js
//= require basic/jquery.blockUI.js
//= require basic/waves.js
//= require basic/jquery.nicescroll.js
//= require basic/jquery.scrollTo.min.js
//= require basic/jquery.core.js
//= require basic/jquery.app.js
