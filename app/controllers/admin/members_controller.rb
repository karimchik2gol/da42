#coding:UTF-8
class Admin::MembersController < AdminController
  before_action :set_member, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /members
  def index
    @members = Member.all
  end

  # GET /members/1
  def show
  end

  # GET /members/new
  def new
    @member = Member.new
    @obj = @member
  end

  # GET /members/1/edit
  def edit
  end

  # POST /members
  def create
    @member = Member.new(member_params)

    if @member.save
      redirect_to admin_member_path(@member), notice: 'Member was successfully created.'
    else
      @errors = @member.errors
      render :new
    end
  end

  # PATCH/PUT /members/1
  def update
    if @member.update(member_params)
      redirect_to admin_member_path(@member), notice: 'Member was successfully updated.'
    else
      @errors = @member.errors
      render :edit
    end
  end

  # DELETE /members/1
  def destroy
    @member.destroy
    redirect_to admin_members_path, notice: 'Member was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
      @errors, @name_model, @obj = @member.errors, "member", @member
    end

    # Only allow a trusted parameter "white list" through.
    def member_params
      params.require(:member).permit(:name, :email, :phone, :age, :gender, :about, :flashmob_id, :password)
    end
end
