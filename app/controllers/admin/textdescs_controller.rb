#coding:UTF-8
class Admin::TextdescsController < AdminController
  before_action :set_textdesc, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /textdescs
  def index
    @textdescs = Textdesc.all
  end

  # GET /textdescs/1
  def show
  end

  # GET /textdescs/new
  def new
    @textdesc = Textdesc.new
    @obj = @textdescs
  end

  # GET /textdescs/1/edit
  def edit
  end

  # POST /textdescs
  def create
    @textdesc = Textdesc.new(textdesc_params)

    if @textdesc.save
      redirect_to admin_textdesc_path(@textdesc), notice: 'Textdesc was successfully created.'
    else
      @errors = @textdesc.errors
      render :new
    end
  end

  # PATCH/PUT /textdescs/1
  def update
    if @textdesc.update(textdesc_params)
      redirect_to admin_textdesc_path(@textdesc), notice: 'Textdesc was successfully updated.'
    else
      @errors = @textdesc.errors
      render :edit
    end
  end

  # DELETE /textdescs/1
  def destroy
    @textdesc.destroy
    redirect_to admin_textdescs_path, notice: 'Textdesc was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_textdesc
      @textdesc = Textdesc.find(params[:id])
      @errors, @name_model, @obj = @textdesc.errors, "textdesc", @textdesc
    end

    # Only allow a trusted parameter "white list" through.
    def textdesc_params
      params.require(:textdesc).permit(:name, :classname, :description, :strip_tags)
    end
end
