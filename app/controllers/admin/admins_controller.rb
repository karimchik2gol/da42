#coding:UTF-8
class Admin::AdminsController < AdminController
  before_action :set_admin, only: [:show, :edit, :update, :destroy]
  before_action :is_batya, only: [:create, :update, :destroy]
  layout "admin"

  # GET /admins
  def index
    @admins = Admin.all
  end

  # GET /admins/1
  def show
  end

  # GET /admins/new
  def new
    @admin = Admin.new
    @obj = @admin
  end

  # GET /admins/1/edit
  def edit
  end

  # POST /admins
  def create
    @admin = Admin.new(admin_params)

    if @admin.save
      redirect_to admin_admin_path(@admin), notice: 'Admin was successfully created.'
    else
      @errors = @admin.errors
      render :new
    end
  end

  # PATCH/PUT /admins/1
  def update
    aParams = Admin.reset_password(admin_params)
    respond_to do |format|
      if @admin.update(aParams)
        sign_in @admin, :bypass => true 
        format.html { redirect_to admin_admin_path(@admin), notice: 'Admin was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin }
      else
        @errors = @admin.errors
        format.html { render :edit }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins/1
  def destroy
    @admin.destroy
    redirect_to admin_admins_path, notice: 'Admin was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def is_batya
      redirect_to admin_admins_path if current_admin.id != Admin.order("created_at ASC").limit(1).first.id && current_admin.id != params[:id].to_i
    end

    def set_admin
      @admin = Admin.find(params[:id])
      @name_model, @obj = @admin.errors, @admin
    end

    # Only allow a trusted parameter "white list" through.
    def admin_params
      params.require(:admin).permit(:name, :image, :image_cache, :email, :password, :password_confirmation)
    end
end
