#coding:UTF-8
class Admin::FlashmobsController < AdminController
  before_action :set_flashmob, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /flashmobs
  def index
    @flashmobs = Flashmob.all
  end

  # GET /flashmobs/1
  def show
  end

  # GET /flashmobs/new
  def new
    @flashmob = Flashmob.new
    @flashmob.build_images
    @obj = @flashmob
  end

  # GET /flashmobs/1/edit
  def edit
    @flashmob.build_images
  end

  # POST /flashmobs
  def create
    @flashmob = Flashmob.new(flashmob_params)

    if @flashmob.save
      redirect_to admin_flashmob_path(@flashmob), notice: 'Flashmob was successfully created.'
    else
      @flashmob.my_build_images(flashmob_params)
      @errors = @flashmob.errors
      render :new
    end
  end

  # PATCH/PUT /flashmobs/1
  def update
    if @flashmob.update(flashmob_params)
      redirect_to admin_flashmob_path(@flashmob), notice: 'Flashmob was successfully updated.'
    else
      @flashmob.my_build_images(flashmob_params)
      @errors = @flashmob.errors
      render :edit
    end
  end

  # DELETE /flashmobs/1
  def destroy
    @flashmob.destroy
    redirect_to admin_flashmobs_path, notice: 'Flashmob was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flashmob
      @flashmob = Flashmob.friendly.find(params[:id])
      @errors, @name_model, @obj = @flashmob.errors, "flashmob", @flashmob
    end

    # Only allow a trusted parameter "white list" through.
    def flashmob_params
      params.require(:flashmob).permit(:name, :location, :expires_at, :short_description, :description, :reports_count, :status, :user_id, :gender, :start_age, :end_age, {:images_attributes => [:id, :image, :image_cache, :_destroy]})
    end
end
