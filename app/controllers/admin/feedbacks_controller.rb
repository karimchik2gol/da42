#coding:UTF-8
class Admin::FeedbacksController < AdminController
  before_action :set_feedback, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /feedbacks
  def index
    @feedbacks = Feedback.all
  end

  # GET /feedbacks/1
  def show
  end

  # GET /feedbacks/new
  def new
    @feedback = Feedback.new
  end

  # GET /feedbacks/1/edit
  def edit
  end

  # POST /feedbacks
  def create
    @feedback = Feedback.new(feedback_params)

    if @feedback.save
      redirect_to admin_feedback_path(@feedback), notice: 'Feedback was successfully created.'
    else
      @feedback = @feedback.errors
      render :new
    end
  end

  # PATCH/PUT /feedbacks/1
  def update
    if @feedback.update(feedback_params)
      redirect_to admin_feedback_path(@feedback), notice: 'Feedback was successfully updated.'
    else
      @feedback = @feedback.errors
      render :edit
    end
  end

  # DELETE /feedbacks/1
  def destroy
    @feedback.destroy
    redirect_to admin_feedbacks_path, notice: 'Feedback was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feedback
      @feedback = Feedback.find(params[:id])
      @errors, @name_model, @obj = @feedback.errors, "feedback", @feedback
    end

    # Only allow a trusted parameter "white list" through.
    def feedback_params
      params.require(:feedback).permit(:name, :email, :phone, :subject, :text)
    end
end
