#coding:UTF-8
class Admin::CommentsController < AdminController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /comments
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
    @obj = @comment
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  def create
    @comment = Comment.new(comment_params)

    if @comment.save
      redirect_to admin_comment_path(@comment), notice: 'Comment was successfully created.'
    else
      @errors = @comment.errors
      render :new
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      redirect_to admin_comment_path(@comment), notice: 'Comment was successfully updated.'
    else
      @errors = @comment.errors
      render :edit
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
    redirect_to admin_comments_path, notice: 'Comment was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
      @errors, @name_model, @obj = @comment.errors, "comment", @comment
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:text, :member_id, :flashmob_id)
    end
end
