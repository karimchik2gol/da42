class ProfileController < ApplicationController
	before_filter :check_user
  	def index
  		@flashmobs = current_user.flashmobs
  		flashmobs_ids = @flashmobs.map { |f| f.id }
  		@popular_flashmob = current_user.flashmobs.popular.first
  	#	@flashmobs = @popular_flashmob ? flsh.where('id != ?', @popular_flashmob.id) : flsh
  		@comments = Comment.where(:flashmob_id => flashmobs_ids)
  		@body_class = "page-template blog"
  	end

    def get_sign_in_panel
      render partial: 'index/sign_in_panel'
    end

  	private
  	def check_user
  		redirect_to root_url unless user_signed_in?
  	end
end
