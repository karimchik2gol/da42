class RegistrationsController < Devise::RegistrationsController
	respond_to :json
	before_filter :configure_permitted_parameters, :only => [:create]
	def create
		unless current_user
			build_resource(sign_up_params)

		    resource.save
		    yield resource if block_given?

		    if resource.persisted?
		      if resource.active_for_authentication?
		        set_flash_message! :notice, :signed_up
		        sign_up(resource_name, resource)
				sign_out @user unless params[:rememberme] == "true"
		        render json: resource.to_json
		        #respond_with resource, location: after_sign_up_path_for(resource)
		      else
		        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
		        expire_data_after_sign_in!
		        respond_with resource, location: after_inactive_sign_up_path_for(resource)
		      end
		    else
		      clean_up_passwords resource
		      set_minimum_password_length
		      render json: resource.errors.to_json, status: 422
		    end
		else
			render json: "You can't register. Already signed in."
		end
	end

	private
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :phone, :email, :password) }
    end
end