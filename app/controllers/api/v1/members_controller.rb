module Api
	module V1
		class MembersController < ApplicationController
			respond_to :json
			skip_before_filter :verify_authenticity_token, only: [:create]

			def create
				flashmob = Flashmob.find(params[:flashmob_id])
				if flashmob && flashmob.approved?
					member = flashmob.members.build(member_params)
					if member.save
						response = { name: member.name, email: member.email, phone: member.phone, gender: member.gender, age: member.age, about: member.about }
						api_key = member.build_api_key
						api_key.save
						response[:access_token] = api_key.access_token
						return render :json => response.to_json
					else
						errors = member.errors.to_json
					end
				else
					errors = {:error => "Hmmm...Look's like we can't find this flashmob or it hadn't been approved"}
				end
				render :json => errors, status: 422
			end

			private
			def member_params
				params.require(:member).permit(:name, :phone, :email, :about, :age, :gender)
			end
		end
	end
end