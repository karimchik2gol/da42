module Api
	module V1
		class AuthorizeController < ApplicationController
			respond_to :json
			protect_from_forgery with: :null_session

			def access_token
				user = User.find_by_email(params[:email])
				if user && user.valid_password?(params[:password])
					opts = { id: user.id, name: user.name, email: user.email, phone: user.phone, created_at: user.created_at, updated_at: user.updated_at, :token => user.api_key.access_token, :status => 200 }
					render :json => opts
				else
					render :json => { :token => "Access denied", :status => 401 }
				end
			end
		end
	end
end