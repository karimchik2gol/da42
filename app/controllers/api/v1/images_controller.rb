module Api
	module V1
		class ImagesController < ApplicationController
			respond_to :json
			protect_from_forgery with: :null_session

			def show
				image = Image.find(params[:id])
				if image.flashmob.approved?
					type = MIME::Types.type_for(image.image_url).first.content_type
					send_file "#{Rails.root}/public#{image.image_url}", type: type, disposition: 'inline'
				else
					render :json => "Sir, i can't give you this image".to_json, status: 422
				end
			end
		end
	end
end