module Api
	module V1
		class FlashmobsController < ApiController
			skip_before_filter :verify_authenticity_token, only: [:create]
			before_action :restrict_access, only: [:create]
			
			def all_flashmobs
				girls = []
				flashmobs = Flashmob.select(:id, :name, :short_description, :description, :status, :user_id, :gender, :start_age, :end_age, :created_at, :location, :slug, :members_count, :expires_at).all
				flashmobs.each do |f|
					girl = f.as_json
					image_attrs = delete_flashmob_id(Image.attribute_names)
					comment_attrs = delete_flashmob_id(Comment.attribute_names)
					girl["images"] = f.images.select(image_attrs).as_json
					girl["comments"] = f.comments.select(comment_attrs).as_json
					girls << girl
				end 
				render :json => girls.to_json
			end

			def create
				user = ApiKey.find_by_access_token(params[:access_token]).user
				flashmob = user.flashmobs.build(flashmob_params)

			    if flashmob.save
			      render :json => "Successful".to_json
			    else
			      render :json => flashmob.errors.to_json, status: 422
			    end
			end

			private
			def delete_flashmob_id(arr)
				return arr.delete_if {|f| f == "flashmob_id" }.map { |f| f.to_sym }
			end
			def flashmob_params
		      params.require(:flashmob).permit(:name, :location, :expires_at, :short_description, :description,:user_id, :gender, :start_age, :end_age, {:images_attributes => [:id, :image, :_destroy]})
		    end
		end
	end
end