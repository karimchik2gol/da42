module Api
	module V1
		class UsersController < ApplicationController
			# OOoops, i need to use OAuth, but not today. Bye!
			def update
				up = User.reset_password(user_params)
				if current_user.update_attributes(up)
					render :json => current_user.to_json
				else
					render :json => current_user.errors.to_json, status: 422
				end
			end

			private
			def user_params
				params.require(:user).permit(:name, :phone, :email, :password, :password_confirmation)
			end
		end
	end
end