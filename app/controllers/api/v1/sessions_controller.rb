module Api
	module V1
		class SessionsController < Devise::SessionsController
			respond_to :json

			def destroy
				super 
			end

			def create
				resource = User.find_for_database_authentication(email: params[:user][:email])
				return invalid_login_attempt unless resource

				if resource.valid_password?(params[:user][:password])
			  		sign_in :user, resource
			  		return render :json => resource.to_json
				end

				invalid_login_attempt
			end

			protected
			def invalid_login_attempt
				render :json => "Oops, an error occured while trying to sign in".to_json, :status => 401
			end
		end
	end
end