module Api
	module V1
		class RegistrationsController < Devise::RegistrationsController
			respond_to :json
			protect_from_forgery with: :null_session

			def create
				build_resource(configure_permitted_parameters)

			    resource.save
			    yield resource if block_given?

			    if resource.persisted?
			        api = resource.build_api_key
			        api.save
			        response = { "access_token": api.access_token }
			        render json: response.to_json
			    else
			      	render json: resource.errors.to_json, status: 422
			    end
			end

			private
		    def configure_permitted_parameters
		      params.require(:user).permit(:name, :phone, :password, :email)
		    end
		end
	end
end