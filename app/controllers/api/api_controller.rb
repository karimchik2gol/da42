module Api
	class ApiController < ApplicationController
		respond_to :json
		protect_from_forgery with: :null_session
		# Api access token
	    def restrict_access
	    	if !params[:access_token] || !ApiKey.exists?(access_token: params[:access_token])
	    		render :json => "HTTP Access denied.".to_json, status: 401
			end
	    end
	end
end