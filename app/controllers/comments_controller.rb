class CommentsController < ApplicationController	
	def show
		@comment = Comment.find(params[:id])	
		render partial: "flashmobs/comment"
	end

	def create
		c = comments_params
		c[:member_id] = session[:member_id]
		c[:flashmob_id] = params[:flashmob_id]
		comment = Comment.create(c)
		if comment.save
			render json: comment.id.to_json
		else
			render json: comment.errors.to_json
		end
	end

	private
	def comments_params
		params.require(:comment).permit(:text)
	end
end