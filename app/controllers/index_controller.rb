class IndexController < ApplicationController	
	def index
		@popular_flashmobs = Flashmob.accessed.popular
		@last_flashmobs = Flashmob.accessed.lastflash(5)
  		@comments = Comment.last_comments

		index = Textdesc.select(:name, :description, :classname).where("name LIKE ?", "Index %")
		@info = index.to_json
	end

	def contact
		contacts = Textdesc.select(:name, :description, :classname).where("name LIKE ?", "Contact %")
		@info = contacts.to_json
	end

	def feedback
		feedback = Feedback.new(feedback_params)
		respond_to do |format|
			format.json {
				if feedback.save
					render :json => nil.to_json
				else
					render :json => feedback.errors.to_json, :status => 422
				end
			}
		end
			
	end

	private
	def feedback_params
		params.require(:feedback).permit(:name, :email, :phone, :subject, :text)
	end
end
