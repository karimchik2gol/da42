class FlashmobsController < ApplicationController	
	include ApplicationHelper

	before_action :set_flashmob, only: [:show, :members, :com, :destroy, :edit, :update]

	ITEMS_ON_PAGE = Flashmob::ITEMS_ON_PAGE
	BLOG_CLASS = 'single single-blog page-template page-template-page-services-php'

	def index
		ransack = Flashmob.accessed.ransack(params[:q])
		@flashmobs = ransack.result.paginate(page: params[:page], per_page: ITEMS_ON_PAGE)

		@q = params[:q]
	end

	def new
		return redirect_to root_url unless current_user
		@flashmob = current_user.flashmobs.build
		@flashmob.build_images
	end

	def edit
		return '/profile' if !current_user || @flashmob.user_id != current_user.id
		@flashmob.build_images
	end

	def create
		@flashmob = current_user.flashmobs.build(flashmob_params)

	    if @flashmob.save
	      redirect_to '/profile', notice: 'Flashmob was successfully created.'
	    else
	      @flashmob.my_build_images(flashmob_params)
	      @errors = @flashmob.errors
	      render :new
	    end
	end

	def update
		return redirect_to '/profile' if current_user.id != @flashmob.user_id
		if @flashmob.update(flashmob_params)
	      redirect_to '/profile', notice: 'Flashmob was successfully updated.'
	    else
	      @flashmob.my_build_images(flashmob_params)
	      @errors = @flashmob.errors
	      render :edit
	    end
	end

	def search
		q_params = clear_params(params[:q])
		q = Flashmob.accessed.ransack(q_params)
		result = q.result
		@flashmobs = result.paginate(page: params[:page], per_page: ITEMS_ON_PAGE)

		if @flashmobs.count > 0
			if (@flashmobs.total_pages - @flashmobs.current_page) > 0
				status = 200
			else # DONT SHOW BUTTON LOAD MORE !!!!
				status = 202
			end
			render partial: 'flashmobs_array', locals: { :ajax => true }, status: status
		else
			render nothing: true, status: 202
		end
	end

	def show
		return redirect_to root_url if !@flashmob.approved? && session[:user_id] != @flashmob.user.id
		@body_class = BLOG_CLASS
		@comments = @flashmob.comments.order('created_at DESC')
	end

	def com
		comment_or_member(@flashmob)
	end

	def members
		return redirect_to root_url unless current_user && current_user.id == @flashmob.user_id
		@members = @flashmob.members
		@body_class = BLOG_CLASS
	end

	def destroy
		if @flashmob.user_id == current_user.id
			@flashmob.destroy
		end
		redirect_to '/profile'
	end

	private
	def set_flashmob
		@flashmob = Flashmob.friendly.find(params[:id])
	end

	def flashmob_params
      params.require(:flashmob).permit(:name, :location, :expires_at, :short_description, :description, :gender, :start_age, :end_age, {:images_attributes => [:id, :image, :image_cache, :_destroy]})
    end

	def clear_params(par)
		par.each do |k,v|
	  		if(v && v.gsub(/\s+/, "").empty?)
	  			par.tap { |hs| hs.delete(k) }
	  		end
	  	end
	  	return par
	end
end