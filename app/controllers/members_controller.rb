class MembersController < ApplicationController
	respond_to :json
	protect_from_forgery with: :null_session
	def create
		flashmob = Flashmob.find(params[:flashmob_id])
		if flashmob && flashmob.approved?
			member = flashmob.members.build(member_params)
			if member.save
				session[:flashmob_id] ||= []
				session[:flashmob_id] << flashmob.id
				session[:member_id] = member.id
				return render :json => member.to_json
			else
				errors = member.errors.to_json
			end
		else
			errors = {:error => "Hmmm...Look's like we can't find this flashmob or it hadn't approved"}
		end
		render :json => errors, status: 422
	end

	private
	def member_params
		params.require(:member).permit(:name, :phone, :email, :about, :age, :gender)
	end
end