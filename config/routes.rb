Rails.application.routes.draw do 
  patch '/flashmob/:id' => 'flashmobs#update'
  get '/flashmob/:id/edit' => 'flashmobs#edit'
  delete '/flashmobs/:id' => 'flashmobs#destroy'
  post '/flashmobs' => 'flashmobs#create'
  get '/flashmob/new' => 'flashmobs#new'
  get '/flashmob/:id/members' => 'flashmobs#members'
  get '/flashmob/:id' => 'flashmobs#show', as: 'flashmob'
  get '/flashmobs/search'
  get '/flashmobs/com'
  get '/flashmobs' => 'flashmobs#index'

  get '/comments/:id' => 'comments#show'
  post '/comments' => 'comments#create'

  get '/contact' => 'index#contact'
  post '/contact' => 'index#feedback'
  
  put 'users' => 'users#update'
  post 'members' => 'members#create'
  # Profile methods
  scope path: :profile, controller: :profile do 
    get '/' => :index
    post '/get_sign_in_panel' => :get_sign_in_panel
  end

  # API
  namespace :api do 
    namespace :v1 do
      get 'image/:id' => 'images#show'
      get 'all_flashmobs' => 'flashmobs#all_flashmobs'
      post 'create' => 'flashmobs#create'
      get 'access_token' => 'authorize#access_token'
      post 'members' => 'members#create'
      post 'add_comment' => 'comments#create'

      # Devise user
      devise_scope :user do
        post '/register' => 'registrations#create'
      end 
    end
  end

  devise_for :users, :skip => ['registrations', 'sessions']
  devise_scope :user do
    post '/users' => 'registrations#create'
    post '/sessions' => 'sessions#create'
    delete '/logout' => 'sessions#destroy'
  end

  # Devise admin
  devise_for :admins, :controllers => {:sessions => 'admin/sessions', :passwords => 'admin/passwords'}
  devise_scope :admin do
    get '/admin' => 'admin/sessions#new'
  end

  # Redirect after devise
  get '/admin/admins', as: 'admin_root', to: 'admin/admins#index'
  
  # Admin
  namespace :admin do
    resources :admins
    resources :textdescs
    resources :flashmobs

    resources :comments
    resources :members
    resources :users
    resources :feedbacks
  end
  
  root to: 'index#index'
end
